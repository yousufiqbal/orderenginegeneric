package orderenginev2

import Callables.FromAffiliateCallable
import Callables.FromStoreCallable
import FirebaseHelper.FireBaseListeners
import FirebaseHelper.FirebaseUtils
import HelpingClasses.ProjectConstants
import Models.FirebaseModels.FromAffiliatePacket
import Models.FirebaseModels.FromStorePacket
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import grails.util.Environment
import org.springframework.beans.factory.annotation.Value

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class BootStrap {


    static ExecutorService fromAffiliatePacketExecutorService

    static ExecutorService fromStorePacketExecutorService

    @Value('${environments.development.dataSource.orderEngineFirebaseKey}')
    String devOrderEngineKey

    @Value('${environments.test.dataSource.orderEngineFirebaseKey}')
    String testOrderEngineKey

    @Value('${environments.production.dataSource.orderEngineFirebaseKey}')
    String prodOrderEngineKey

    def init = { servletContext ->
        String orderEngineKey = null
        String orderEngineFirebase = null

        fromAffiliatePacketExecutorService = Executors.newFixedThreadPool(20)

        fromStorePacketExecutorService = Executors.newFixedThreadPool(20)
        if(Environment.current==Environment.DEVELOPMENT){
            orderEngineKey = devOrderEngineKey
            orderEngineFirebase = ProjectConstants.devOrderEngineFirebase
        }
        else if(Environment.current==Environment.TEST) {
            orderEngineKey = testOrderEngineKey
            orderEngineFirebase = ProjectConstants.testOrderEngineFirebase
        }

        else if(Environment.current==Environment.PRODUCTION) {
            orderEngineKey = prodOrderEngineKey
            orderEngineFirebase = ProjectConstants.prodOrderEngineFirebase
        }

        if(orderEngineFirebase!=null && orderEngineKey!=null) {

            InputStream inputStream = this.class.classLoader.getResourceAsStream('data/' + orderEngineKey)


            FirebaseUtils.initializeFirebase(inputStream, orderEngineFirebase)


            FirebaseUtils.getFromAffiliateRef().addChildEventListener(new ChildEventListener() {
                @Override
                void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                    FromAffiliatePacket fromAffiliatePacket = snapshot.getValue(FromAffiliatePacket.class)
                    String key = snapshot.getKey()
                    FromAffiliateCallable fromAffiliateCallable = new FromAffiliateCallable(fromAffiliatePacket, key)
                    fromAffiliatePacketExecutorService.submit(fromAffiliateCallable)
                }

                @Override
                void onChildChanged(DataSnapshot snapshot, String previousChildName) {

                }

                @Override
                void onChildRemoved(DataSnapshot snapshot) {

                }

                @Override
                void onChildMoved(DataSnapshot snapshot, String previousChildName) {

                }

                @Override
                void onCancelled(DatabaseError error) {

                }
            })


            FirebaseUtils.getFromStoreRef().addChildEventListener(new ChildEventListener() {
                @Override
                void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                    FromStorePacket fromStorePacket = dataSnapshot.getValue(FromStorePacket.class)
                    String key = dataSnapshot.getKey()
                    FromStoreCallable fromStoreCallable = new FromStoreCallable(fromStorePacket, key)
                    fromStorePacketExecutorService.submit(fromStoreCallable)
                }

                @Override
                void onChildChanged(DataSnapshot snapshot, String previousChildName) {

                }

                @Override
                void onChildRemoved(DataSnapshot snapshot) {

                }

                @Override
                void onChildMoved(DataSnapshot snapshot, String previousChildName) {

                }

                @Override
                void onCancelled(DatabaseError error) {

                }
            })
        }

    }

    def destroy = {

        FirebaseUtils.getFromAffiliateRef().removeEventListener(FireBaseListeners.fromAffiliateChildEventListener)

        FirebaseUtils.getFromStoreRef().removeEventListener(FireBaseListeners.fromStoreChildEventListener)

        fromAffiliatePacketExecutorService.shutdownNow()

        fromStorePacketExecutorService.shutdownNow()
    }

}
