package HelpingClasses.RestApiServices

import Models.kafka.TopicData
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by yousu on 10/24/2017.
 */
interface WebServiceProxy {
    @GET("/spec/get")
    Call<ResponseBody> getSpecFromNifi(@Header("storeid-entity") String transformation_id)


    @POST("/speclist/transform")
    Call<ResponseBody> transfromViaNifiList(@Body List<HashMap<String,Object>> listEntities,
                                            @Header("storeid-entity") String transformation_id,
                                            @Header("Content-Type") String content_type )


    @POST("/spec/transform")
    Call<ResponseBody> transfromViaNifiSingle(  @Body HashMap<String,Object> entity,
                                                @Header("storeid-entity") String transformation_id )


    @POST("/topics/{topic_name}")
    Call<ResponseBody> postOrderToTopic(@Path("topic_name") String topic_name,
                                        @Body TopicData topicData,
                                        @Header("Content-Type") String content_type )

}
