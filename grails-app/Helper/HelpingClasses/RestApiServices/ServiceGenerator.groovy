package HelpingClasses.RestApiServices


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

import java.util.concurrent.TimeUnit

/**
 * Created by yousu on 10/24/2017.
 */
class ServiceGenerator  {
    static final String TAG = ServiceGenerator.class.getSimpleName()

    static WebServiceProxy getWebServiceProxy(String endPoint){

        // println("ServiceGenerator : " + endPoint)

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        // init cookie manager
        CookieHandler cookieHandler = new CookieManager()

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build()


//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create()

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endPoint)
                .client(okHttpClient)
        // .addConverterFactory(SimpleXmlConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()

        return retrofit.create( WebServiceProxy.class)
    }


    static WebServiceProxy getWebServiceProxyGson(String endPoint){

        // println("ServiceGenerator : " + endPoint)

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        // init cookie manager
        CookieHandler cookieHandler = new CookieManager()

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build()


        Gson gson = new GsonBuilder()
                .setLenient()
                .create()

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endPoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        return retrofit.create( WebServiceProxy.class)
    }


    static WebServiceProxy getWebServiceProxyWithNull(String endPoint){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        // init cookie manager
        CookieHandler cookieHandler = new CookieManager()

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build()


        println(TAG +" : " +  "endPoint: " + endPoint)

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setLenient()
                .create()

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endPoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        return retrofit.create( WebServiceProxy.class)
    }

}
