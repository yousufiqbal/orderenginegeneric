package HelpingClasses

import HelpingClasses.RestApiServices.ServiceGenerator
import HelpingClasses.RestApiServices.WebServiceProxy
import com.bazaarvoice.jolt.Chainr
import com.bazaarvoice.jolt.JsonUtils
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

/**
 * Created by yousu on 10/24/2017.
 */
class SpecUtils {
    static  def specTransform(String spec,Object object){

        List chainrSpecJSON = JsonUtils.jsonToList(spec)

        Chainr chainr = Chainr.fromSpec(chainrSpecJSON)

        Object  objectToReturn = chainr.transform(object)
        return objectToReturn

    }

    static String getSpecFromNifi(String specId){

        WebServiceProxy webServiceProxy = ServiceGenerator.getWebServiceProxyGson(ProjectConstants.nifiLink)

        Call<ResponseBody> call = webServiceProxy.getSpecFromNifi(specId)

        Response<ResponseBody> response = call.execute()

        if(response.code()!=200){
            Thread.sleep((ProjectConstants.secondInMilli*10).longValue())
            CustomLogger.createAndSendLog("Nifi Error for spec " + specId,"none","none","Error "+response.code())
            return getSpecFromNifi(specId)
        }

        ResponseBody responseBody = response.body()

        String spec = responseBody.string()

        return spec



    }
}
