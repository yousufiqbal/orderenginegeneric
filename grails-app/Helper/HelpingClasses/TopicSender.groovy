package HelpingClasses

import HelpingClasses.RestApiServices.ServiceGenerator
import HelpingClasses.RestApiServices.WebServiceProxy
import Models.kafka.Record
import Models.kafka.TopicData
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

class TopicSender {

    static void postDataToTopic(String payload, String topic){

        WebServiceProxy webServiceProxy = ServiceGenerator.getWebServiceProxyWithNull(ProjectConstants.kafkaEndPoint)

        TopicData topicData = new TopicData()

        Record record = new Record()

        record.value.payload = payload

        topicData.records.add(record)

        String dataToSend = new Gson().toJson(topicData)

        println(dataToSend)

        Call<ResponseBody> call = webServiceProxy.postOrderToTopic(topic, topicData, ProjectConstants.kafkaContentType )

        Response<ResponseBody> response = call.execute()

        if(response.isSuccessful()){

            ResponseBody responseBody = response.body()

            String responseBodyString = responseBody.string()

            print(responseBodyString)

//            ExceptionUtils.getInstance(apiInfo).log(TAG,"response : " + responseBodyString)
        }
        else {

            String responseString = response.errorBody().string()
//            ExceptionUtils.getInstance(apiInfo).log(TAG,"response error: " + responseString)
            throw new Exception(responseString)
        }

    }

}
