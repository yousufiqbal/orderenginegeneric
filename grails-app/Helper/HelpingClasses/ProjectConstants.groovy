package HelpingClasses
/**
 * Created by yousu on 10/24/2017.
 */
class ProjectConstants {
    static String prodOrderEngineFirebase =  "https://orderengine-476a6.firebaseio.com/"
    static String devOrderEngineFirebase =  "https://orderengine-cbb6f.firebaseio.com/"
    static String testOrderEngineFirebase =  "https://orderengine-cbb6f.firebaseio.com/"

  

    public static long secondInMilli = 1000

    public static long minuteInMilli = 60000

    public static final String nifiLink = "http://technify.tech:61001/"

    public static String orderSpecsIdForNifi = "orderEngineV2"
    public static String orderEngineId = "0"


    static final String kafkaEndPoint = "http://technify.tech:62111"
//    static final String kafkaEndPoint = "http://104.154.82.213:8082"


    static final String kafkaContentType = "application/vnd.kafka.json.v2+json"

}
