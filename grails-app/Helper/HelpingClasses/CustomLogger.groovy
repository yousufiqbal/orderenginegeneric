package HelpingClasses

import com.google.gson.Gson

/**
 * Created by yousu on 10/24/2017.
 */
class CustomLogger {
    public static void sendLog(def message) {
        println(message)
        try {
            def socket = new DatagramSocket()
            def data = message.toString().getBytes("ASCII")
            def packet = new DatagramPacket(data, data.length, InetAddress.getByName("dev.technify.pk"), 2214)
            socket.send(packet)
        }
        catch (Exception e)
        {

        }
    }
    public static void createAndSendLog(def process,def affiliateId,def storeId,def message) {
        println(message)
        try {
            Map logsMap = new HashMap()

            logsMap.put("Project","OrderEngine")
            logsMap.put("message",message)
            logsMap.put("process",process)
            logsMap.put("storeId",storeId)
         
            String MessageToSend = new Gson().toJson(logsMap).toString()



            def socket = new DatagramSocket()
            def data = MessageToSend.getBytes("ASCII")
            def packet = new DatagramPacket(data, data.length, InetAddress.getByName("dev.technify.pk"), 2214)
            socket.send(packet)
        }
        catch (Exception e)
        {

        }
    }
}