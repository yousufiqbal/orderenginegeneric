package Callables

import ActionHandlers.FromAffiliateHandler
import FirebaseHelper.FirebaseUtils
import Models.FirebaseModels.FromAffiliatePacket

import java.util.concurrent.Callable

/**
 * Created by yousu on 10/24/2017.
 */
class FromAffiliateCallable implements Callable {
    FromAffiliatePacket fromAffiliatePacket
    String firebaseKey
    FromAffiliateCallable(FromAffiliatePacket fromAffiliatePacket,String firebaseKey){
        this.fromAffiliatePacket = fromAffiliatePacket
        this.firebaseKey = firebaseKey
    }
    @Override
    Object call() throws Exception {
    try {
        FromAffiliateHandler fromAffiliateHandler = new FromAffiliateHandler(fromAffiliatePacket)

        fromAffiliateHandler.handleActionFromAffiliate()


        FirebaseUtils.getFromAffiliateRef().child(firebaseKey).removeValueAsync()
    }
    catch (Exception e){
        e.printStackTrace()
    }
        return null
    }
}
