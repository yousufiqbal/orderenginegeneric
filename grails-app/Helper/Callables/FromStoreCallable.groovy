package Callables

import ActionHandlers.FromStoreHandler
import FirebaseHelper.FirebaseUtils
import Models.FirebaseModels.FromStorePacket

import java.util.concurrent.Callable

/**
 * Created by yousu on 10/24/2017.
 */
class FromStoreCallable implements Callable{
    FromStorePacket fromStorePacket
    String firebaseKey

    FromStoreCallable(FromStorePacket fromStorePacket,String firebaseKey){
        this.fromStorePacket = fromStorePacket
        this.firebaseKey = firebaseKey
    }
    @Override
    Object call() throws Exception {
        try {
            FromStoreHandler fromStoreHandler = new FromStoreHandler(fromStorePacket)

            fromStoreHandler.handleActionFromStore()
            FirebaseUtils.getFromStoreRef().child(firebaseKey).removeValueAsync()
            return null
        }
        catch (Exception e){
            e.printStackTrace()
        }
    }
}
