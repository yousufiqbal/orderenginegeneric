package DBHelpers

import orderenginev2.DBAffiliateOrder

class DBAffiliateOrderDBHelper {

    private static DBAffiliateOrderDBHelper dbAffiliateOrderDBHelper

    private DBAffiliateOrderDBHelper(){

    }

    public static DBAffiliateOrderDBHelper getInstance() {
        if(dbAffiliateOrderDBHelper==null){
            dbAffiliateOrderDBHelper = new DBAffiliateOrderDBHelper()
        }
        return dbAffiliateOrderDBHelper
    }

    static DBAffiliateOrder saveAffiliateOrder(DBAffiliateOrder dbAffiliateOrder){

        DBAffiliateOrder tempDbAffiliateOrder = DBAffiliateOrder
                .findByAffiliateOrderIdAndAffiliateId(dbAffiliateOrder.affiliateOrderId, dbAffiliateOrder.affiliateId)

        if(tempDbAffiliateOrder == null){
            dbAffiliateOrder.action = "insertOrder"
            DBAffiliateOrder.withTransaction {
                dbAffiliateOrder.save(flush:true)
            }
        }

        else{

            tempDbAffiliateOrder.action = "updateOrder"

            tempDbAffiliateOrder.orderStatusId = dbAffiliateOrder.orderStatusId

            tempDbAffiliateOrder.status = dbAffiliateOrder.status

            tempDbAffiliateOrder.affiliateOrderObj = dbAffiliateOrder.affiliateOrderObj
            DBAffiliateOrder.withTransaction {
                tempDbAffiliateOrder.save(flush:true)
            }
            dbAffiliateOrder = tempDbAffiliateOrder
        }
        return dbAffiliateOrder

    }

}
