package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingMethod {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("code")
    @Expose
    public String code;

    @SerializedName("shipping_title")
    @Expose
    public String shipping_title;
}
