package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Total {
    @SerializedName("order_total_id")
    @Expose
    public String order_total_id;

    @SerializedName("order_id")
    @Expose
    public String order_id;

    @SerializedName("code")
    @Expose
    public String code;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("value")
    @Expose
    public String value;

    @SerializedName("sort_order")
    @Expose
    public String sort_order;
}
