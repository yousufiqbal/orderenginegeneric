package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("customer_id")
    @Expose
    public String customer_id;

    @SerializedName("customer_group_id")
    @Expose
    public String customer_group_id;

    @SerializedName("firstname")
    @Expose
    public String firstname;

    @SerializedName("lastname")
    @Expose
    public String lastname;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("telephone")
    @Expose
    public String telephone;
}
