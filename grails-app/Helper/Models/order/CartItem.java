package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CartItem implements  Cloneable{

    @Override
    public CartItem clone() throws CloneNotSupportedException {
        return (CartItem)super.clone();
    }

    @SerializedName("product_id")
    @Expose
    public String product_id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("quantity")
    @Expose
    public String quantity;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("product_total")
    @Expose
    public String product_total;

    @SerializedName("options")
    public List<CartItemOption> options = new ArrayList<>();
}
