package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingAddress {
    @SerializedName("firstname")
    @Expose
    public String firstname;

    @SerializedName("lastname")
    @Expose
    public String lastname;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("postcode")
    @Expose
    public String postcode;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("country_id")
    @Expose
    public String country_id;

    @SerializedName("country_iso_code_2")
    @Expose
    public String country_iso_code_2;

    @SerializedName("country_iso_code_3")
    @Expose
    public String country_iso_code_3;

    @SerializedName("zone")
    @Expose
    public String zone;

    @SerializedName("zone_id")
    @Expose
    public String zone_id;

    @SerializedName("zone_iso_code")
    @Expose
    public String zone_iso_code;
}
