package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderInfo {
    @SerializedName("order_id")
    @Expose
    public String order_id;

    @SerializedName("order_number")
    @Expose
    public String order_number;

    @SerializedName("invoice_no")
    @Expose
    public String invoice_no;

    @SerializedName("invoice_prefix")
    @Expose
    public String invoice_prefix;

    @SerializedName("store_id")
    @Expose
    public String store_id;

    @SerializedName("store_name")
    @Expose
    public String store_name;

    @SerializedName("store_url")
    @Expose
    public String store_url;

    @SerializedName("currency_code")
    @Expose
    public String currency_code;

    @SerializedName("currecny_id")
    @Expose
    public String currecny_id;

    @SerializedName("currency_value")
    @Expose
    public String currency_value;

    @SerializedName("date_added")
    @Expose
    public String date_added;

    @SerializedName("date_modified")
    @Expose
    public String date_modified;
}
