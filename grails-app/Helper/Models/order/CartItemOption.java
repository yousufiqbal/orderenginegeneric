package Models.order;

import com.google.gson.annotations.SerializedName;

public class CartItemOption {

    @SerializedName("option_id")
    public String option_id;

    @SerializedName("option_value_id")
    public String option_value_id;

    @SerializedName("name")
    public String name;

    @SerializedName("value")
    public String value;

    @SerializedName("type")
    public String type;

    @SerializedName("price")
    public String price;

}
