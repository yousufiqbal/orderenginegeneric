package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethod {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("product_ids")
    @Expose
    public List<String> product_ids = new ArrayList<>();



    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("payment_title")
    @Expose
    public String payment_title;

}
