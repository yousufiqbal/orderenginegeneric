package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Order implements Cloneable{

    @Override
    public Order clone() throws CloneNotSupportedException {
        return (Order) super.clone();
    }

    @SerializedName("order_info")
    public OrderInfo order_info;

    @SerializedName("customer")
    @Expose
    public Customer customer;

    @SerializedName("payment_address")
    @Expose
    public PaymentAddress payment_address;

    @SerializedName("shipping_address")
    @Expose
    public ShippingAddress shipping_address;

    @SerializedName("cart")
    @Expose
    public List<CartItem> cart;

    @SerializedName("payment_method")
    @Expose
    public PaymentMethod payment_method;

    @SerializedName("shipping_method")
    @Expose
    public List<ShippingMethod> shipping_method;

    @SerializedName("total")
    public List<Total> total;

    @SerializedName("order_history")
    public OrderHistory order_history;

}
