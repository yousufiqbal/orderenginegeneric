package Models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistory {
    @SerializedName("date_format")
    @Expose
    public String date_format;

    @SerializedName("date_added")
    @Expose
    public String date_added;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("order_status_id")
    @Expose
    public String order_status_id;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("notify")
    @Expose
    public String notify;
}
