package Models.kafka

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TopicData {
    @SerializedName("records")
    @Expose
    private List<Record> records = new ArrayList<>();

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
