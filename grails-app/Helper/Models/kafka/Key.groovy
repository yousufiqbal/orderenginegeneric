package Models.kafka

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Key {


    @SerializedName("schema")
    @Expose
    private Object schema;
    @SerializedName("payload")
    @Expose
    private Object payload;





    public Object getSchema() {
        return schema;
    }

    public void setSchema(Object schema) {
        this.schema = schema;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

}
