package Models.kafka

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Value {
    @SerializedName("schema")
    @Expose
    private Schema schema;

    @SerializedName("payload")
    @Expose
    private String payload;




    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
