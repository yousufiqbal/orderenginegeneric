package Models.kafka

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Record {


    @SerializedName("key")
    @Expose
    private Key key;
    @SerializedName("value")
    @Expose
    private Value value;


    public Record(){
        this.key = new Key();
        this.key.setPayload(null);
        this.key.setSchema(null);

        value = new Value();
        Schema schema = new Schema();
        schema.setOptional(false);
        schema.setType("string");
        value.setSchema(schema);
    }



    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
