package Models.kafka

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Schema {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("optional")
    @Expose
    private Boolean optional;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getOptional() {
        return optional;
    }

    public void setOptional(Boolean optional) {
        this.optional = optional;
    }
}
