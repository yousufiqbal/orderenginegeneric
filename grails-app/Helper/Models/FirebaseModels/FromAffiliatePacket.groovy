package Models.FirebaseModels

import com.google.gson.annotations.SerializedName

/**
 * Created by yousu on 10/24/2017.
 */
class FromAffiliatePacket {
    @SerializedName("affiliate_id")
    public String affiliate_id

    @SerializedName("affiliate_order_id")
    public String affiliate_order_id

    @SerializedName("action")
    public String action

    @SerializedName("order_json")
    public String order_json


}
