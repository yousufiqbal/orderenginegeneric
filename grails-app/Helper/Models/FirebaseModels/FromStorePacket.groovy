package Models.FirebaseModels

import com.google.gson.annotations.SerializedName

/**
 * Created by yousu on 10/24/2017.
 */
class FromStorePacket {
    @SerializedName("action")
    public String action

    @SerializedName("store_id")
    public String store_id

    @SerializedName("store_order_id")
    public String store_order_id

    @SerializedName("order_json")
    public String order_json

    @SerializedName("id_to_return")
    public String id_to_return



}
