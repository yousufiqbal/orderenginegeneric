package FirebaseHelper

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

/**
 * Created by yousu on 10/24/2017.
 */
class FirebaseUtils {
    static FirebaseDatabase database

    static initializeFirebase(InputStream inputStream, String databaseUrl) {

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(inputStream))
                .setDatabaseUrl(databaseUrl)
                .build()

        if (FirebaseApp.apps != null && FirebaseApp.apps.size() == 0) {
            FirebaseApp.initializeApp(options)
        }
    }

    private static FirebaseDatabase getDatabase() {

        if (database == null) {
            database = FirebaseDatabase.getInstance()
        }

        return database
    }

    public static DatabaseReference getBaseRef(){
        return getDatabase().getReference()
    }

    static DatabaseReference getPresenceListRef() {
        return getBaseRef().child("Presence")
    }

    private static DatabaseReference getStoreBaseRef(){
        return getBaseRef().child("Store")
    }

    private static DatabaseReference getAffiliateBaseRef(){
        return getBaseRef().child("Affiliate")
    }

    static DatabaseReference getFromStoreRef(){
        return getStoreBaseRef().child("FromStore")
    }

    static DatabaseReference getToStoreRef(){
        return getStoreBaseRef().child("ToStore")
    }

    static DatabaseReference getFromAffiliateRef(){
        return getAffiliateBaseRef().child("FromAffiliate")
    }

    static DatabaseReference getFaultyNodesFromStoreRef() {
        return getBaseRef().child("FaultyNodes").child("FromStore")
    }

    static DatabaseReference getFaultyNodesToStoreRef() {
        return getBaseRef().child("FaultyNodes").child("ToStore")
    }


    static DatabaseReference getFaultyNodesFromAffiliateRef() {
        return getBaseRef().child("FaultyNodes").child("FromAffiliate")
    }

    static DatabaseReference getStoreResponse(){
        return getStoreBaseRef().child("StoreResponse")
    }

    static DatabaseReference getToAffiliateRef(){
        return getAffiliateBaseRef().child("ToAffiliate")
    }
}
