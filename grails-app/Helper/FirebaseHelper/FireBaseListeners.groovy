package FirebaseHelper

import Callables.FromAffiliateCallable
import Callables.FromStoreCallable
import Models.FirebaseModels.FromAffiliatePacket
import Models.FirebaseModels.FromStorePacket
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import orderenginev2.BootStrap

/**
 * Created by yousu on 10/24/2017.
 */
class FireBaseListeners {
    static ChildEventListener fromAffiliateChildEventListener = new ChildEventListener() {
        @Override
        void onChildAdded(DataSnapshot dataSnapshot, String s) {


            FromAffiliatePacket fromAffiliatePacket = dataSnapshot.getValue(FromAffiliatePacket.class)
            String key = dataSnapshot.getKey()
            FromAffiliateCallable fromAffiliateCallable = new FromAffiliateCallable(fromAffiliatePacket,key)
            BootStrap.fromAffiliatePacketExecutorService.submit(fromAffiliateCallable)


        }

        @Override
        void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        void onCancelled(DatabaseError databaseError) {

        }
    }



    static  ChildEventListener fromStoreChildEventListener = new ChildEventListener() {
        @Override
        void onChildAdded(DataSnapshot dataSnapshot, String s) {
            FromStorePacket fromStorePacket = dataSnapshot.getValue(FromStorePacket.class)
            String key = dataSnapshot.getKey()
            FromStoreCallable fromStoreCallable = new FromStoreCallable(fromStorePacket,key)
            BootStrap.fromStorePacketExecutorService.submit(fromStoreCallable)
        }

        @Override
        void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        void onCancelled(DatabaseError databaseError) {

        }
    }
}
