package ActionHandlers

import DBHelpers.DBAffiliateOrderDBHelper
import HelpingClasses.ProjectConstants
import HelpingClasses.RestApiServices.WebServiceProxy
import HelpingClasses.SpecUtils
import HelpingClasses.TopicSender
import Models.FirebaseModels.FromAffiliatePacket
import Models.order.CartItem
import Models.order.Order
import Models.order.ShippingMethod
import Models.order.Total
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import orderenginev2.DBAffiliateOrder
import orderenginev2.StoreOrder

/**
 * Created by yousu on 10/24/2017.
 */
class FromAffiliateHandler {

    static enum SpecNames{
        ORDER("order"),
        ORDER_SPEC_FOR_STORE("orderSpecForStore"),
        RELATION_WITH_STORE("relationsWithStore"),
        INFO_CHANGE_SPEC_IF_MARKETPLACE("infoChangeSpecIfMarketPlace")

        SpecNames(String value) {
            this.value = value
        }
        private final String value
        String getValue() {
            value
        }
    }


    FromAffiliatePacket fromAffiliatePacket
    FromAffiliateHandler(FromAffiliatePacket fromAffiliatePacket){
        this.fromAffiliatePacket = fromAffiliatePacket
    }


    HashMap<String,Integer> AffiliateRelation = null

    String INFO_CHANGE_SPEC_IF_MARKETPLACE_STRING = null

    def handleActionFromAffiliate(){
        Order order
        try {
            order = new Gson().fromJson(fromAffiliatePacket.order_json, new TypeToken<Order>() {}.getType())


            String totalString = getTotal(order)

            String orderJson = fromAffiliatePacket.order_json

            DBAffiliateOrder dbAffiliateOrder = new DBAffiliateOrder(
                    fromAffiliatePacket.affiliate_id,
                    fromAffiliatePacket.affiliate_order_id,
                    order.order_history.status,
                    order.order_history.order_status_id,
                    totalString,
                    orderJson)




            dbAffiliateOrder = DBAffiliateOrderDBHelper.saveAffiliateOrder(dbAffiliateOrder)


            HashMap<String,Order> ordersForEachStore = getOrdersForEachStore(order)

            List<StoreOrder> dbStoreOrderList = new ArrayList<>()


            for(String key:ordersForEachStore.keySet()){

                Order tempOrder = ordersForEachStore.get(key)

                StoreOrder dbStoreOrder = new StoreOrder()

                dbStoreOrder.storeId = key

                dbStoreOrder.status = tempOrder.order_history.status

                dbStoreOrder.statusId = tempOrder.order_history.order_status_id

                dbStoreOrder.affiliateOrderId = dbAffiliateOrder.affiliateOrderId

                dbStoreOrder.affiliateId = dbAffiliateOrder.affiliateId

                if(dbAffiliateOrder.action.equals("updateOrder")){
                    StoreOrder storeOrder = StoreOrder.findByAffiliateIdAndAffiliateOrderIdAndStoreId(fromAffiliatePacket.affiliate_id,
                            fromAffiliatePacket.affiliate_order_id,
                            dbStoreOrder.storeId)
                    if(storeOrder!=null){
                        tempOrder.order_info.order_id = storeOrder.storeOrderId
                    }
                }

                dbStoreOrder.storeOrderObj = new Gson().toJson(tempOrder)

                dbStoreOrderList.add(dbStoreOrder)

            }





            if(dbAffiliateOrder.action.equals("insertOrder")){

                dbStoreOrderList = createNewStoreOrders(dbStoreOrderList)

                for(StoreOrder dbStoreOrder : dbStoreOrderList){
                    sendToStore(dbStoreOrder,"insertOrder")
                }

            }

            else {

                dbStoreOrderList = updateStoreOrders(dbStoreOrderList)
                for(StoreOrder dbStoreOrder : dbStoreOrderList){
                    if(!dbStoreOrder.storeOrderId.equals(""))
                        sendToStore(dbStoreOrder,"updateOrder")
                    else
                        sendToStore(dbStoreOrder,"insertOrder")
                }

            }


        }
        catch (Exception e){

            e.printStackTrace()
        }






    }

    def sendToStore(StoreOrder dbStoreOrder, String action) {

        HashMap<String,Object> recordValue = new HashMap<>()

        recordValue.put("action",action)

        recordValue.put("store_id",dbStoreOrder.storeId)

        recordValue.put("id_to_return",dbStoreOrder.id.toString())

        LinkedTreeMap<String,Object> linkedTreeMapOrder = getOrderConvertedAccordingToStoreRelationWithAffiliate(dbStoreOrder)

        recordValue.put("order_json",linkedTreeMapOrder)

        String payload = new Gson().toJson(recordValue)

//        TopicSender.postDataToTopic(payload,"store_"+dbStoreOrder.storeId)

        TopicSender.postDataToTopic(payload,"store_"+dbStoreOrder.storeId)

    }

     protected LinkedTreeMap<String,Object> getOrderConvertedAccordingToStoreRelationWithAffiliate(StoreOrder storeOrder) {


         String spec = SpecUtils.getSpecFromNifi(getSpecName(SpecNames.ORDER_SPEC_FOR_STORE.getValue()))



         HashMap<String,Object> hashMap = new Gson().fromJson(storeOrder.storeOrderObj,new TypeToken<HashMap<String,Object>>(){}.getType())

         hashMap.put("affiliate_order_id",storeOrder.affiliateOrderId)

         Object tempOrderForStore = SpecUtils.specTransform(spec,hashMap)

         if(AffiliateRelation.equals(null)) {

             spec = SpecUtils.getSpecFromNifi(getSpecName(SpecNames.RELATION_WITH_STORE.getValue(), "affiliate_" + fromAffiliatePacket.affiliate_id))

             AffiliateRelation = new Gson().fromJson(spec,new TypeToken<HashMap<String,Integer>>(){}.getType())

         }

         if(AffiliateRelation.get(storeOrder.storeId).equals(0)){

             if(INFO_CHANGE_SPEC_IF_MARKETPLACE_STRING.equals(null)) {

                 INFO_CHANGE_SPEC_IF_MARKETPLACE_STRING = SpecUtils.getSpecFromNifi(getSpecName(SpecNames.INFO_CHANGE_SPEC_IF_MARKETPLACE.getValue(),
                         "affiliate_" + fromAffiliatePacket.affiliate_id))

             }

             tempOrderForStore = SpecUtils.specTransform(INFO_CHANGE_SPEC_IF_MARKETPLACE_STRING,tempOrderForStore)

         }


         LinkedTreeMap<String,Object> objectToReturn = new Gson().fromJson(new Gson().toJson(tempOrderForStore),new TypeToken<LinkedTreeMap<String,Object>>(){}.getType())

         return objectToReturn


     }

    List<StoreOrder> updateStoreOrders(ArrayList<StoreOrder> dbStoreOrderList) {
        try{

        List<StoreOrder>  tempDBStoreOrderList = StoreOrder
                .findAllByAffiliateIdAndAffiliateOrderId(fromAffiliatePacket.affiliate_id,fromAffiliatePacket.affiliate_order_id).sort({it->it.createdAt})

        HashMap<String,StoreOrder> dbStoreOrderHashMap = new HashMap<>()

        List<StoreOrder> listToReturn = new ArrayList<>()

        for(StoreOrder dbStoreOrder:tempDBStoreOrderList){
            if(dbStoreOrderHashMap.containsKey(dbStoreOrder.storeId)){
                dbStoreOrderHashMap.get(dbStoreOrder.storeId).delete()
                dbStoreOrderHashMap.put(dbStoreOrder.storeId,dbStoreOrder)
            }
            else {
                dbStoreOrderHashMap.put(dbStoreOrder.storeId,dbStoreOrder)
            }
        }

        for(StoreOrder dbStoreOrder:dbStoreOrderList){

            if(dbStoreOrderHashMap.containsKey(dbStoreOrder.storeId)){
                if(!dbStoreOrderHashMap.get(dbStoreOrder.storeId).storeOrderId.equals("")) {
                    StoreOrder tempDBStoreOrder = dbStoreOrderHashMap.get(dbStoreOrder.storeId)

                    tempDBStoreOrder.status = dbStoreOrder.status

                    tempDBStoreOrder.statusId = dbStoreOrder.statusId

                    tempDBStoreOrder.storeOrderObj = dbStoreOrder.storeOrderObj

                    tempDBStoreOrder.updatedAt = Calendar.getInstance().getTime().getTime() / 1000

                    listToReturn.add(tempDBStoreOrder)

                }
                else{
                    //Error no response recieved from store yet
                }
            }
            else{

                listToReturn.add(dbStoreOrder)

            }

        }

            StoreOrder.withTransaction {
                StoreOrder.saveAll(listToReturn)
        }
            return listToReturn
        }
        catch (Exception e){
            e.printStackTrace()
        }
        return null


    }

    List<StoreOrder> createNewStoreOrders(List<StoreOrder> dbStoreOrderList) {

        try {
            List<StoreOrder> tempDBStoreOrderList = StoreOrder
                    .findAllByAffiliateIdAndAffiliateOrderId(fromAffiliatePacket.affiliate_id, fromAffiliatePacket.affiliate_order_id)


            StoreOrder.deleteAll(tempDBStoreOrderList)

            StoreOrder.withTransaction {
                StoreOrder.saveAll(dbStoreOrderList)
            }

            //dbStoreOrderList = DBAffiliateOrderDBHelper.getInstance().createNewStoreOrder(dbStoreOrderList,tempDBStoreOrderList)
            return dbStoreOrderList
        }
        catch (Exception e){
            e.printStackTrace()
        }
        return null


    }

    static HashMap<String,Order> getOrdersForEachStore(Order order) {

        HashMap<String,Order> orderHashMapToReturn = new HashMap<>()

        if(order.cart != null) {

            HashMap<String, List<CartItem>> cartsForEachStore = getCartsForEachStore(order.cart)

            HashMap<String, List<ShippingMethod>> shippingMethodsForEachStore = null

            if(order.shipping_method != null) {
                shippingMethodsForEachStore = getShippingMethodsForEachStore(order.shipping_method)
            }

            for(String key: cartsForEachStore.keySet()){

                Order tempOrder = order.clone()

                tempOrder.cart = cartsForEachStore.get(key)

                if(shippingMethodsForEachStore!=null) {

                    if(shippingMethodsForEachStore.containsKey(key))
                        tempOrder.shipping_method = shippingMethodsForEachStore.get(key)
                    else if(shippingMethodsForEachStore.containsKey("0"))
                        tempOrder.shipping_method = shippingMethodsForEachStore.get("0")

                }
                orderHashMapToReturn.put(key,tempOrder)

            }

            return orderHashMapToReturn
            



        }
        else{

            return null

        }
    }

    static HashMap<String,List<ShippingMethod>> getShippingMethodsForEachStore(List<ShippingMethod> shippingMethods) {

        HashMap<String,List<ShippingMethod>> shippingMethodsForEachStore = new HashMap<>()

        for(ShippingMethod shippingMethod : shippingMethods){

            if(shippingMethod.id.equals("0")){

                def tempAwamiShipping = new ArrayList<ShippingMethod>()

                tempAwamiShipping.add(shippingMethod)

                shippingMethodsForEachStore.put("0",tempAwamiShipping)

            }

            else {
                String storeId = shippingMethod.id.substring(0, 6)

                if (!shippingMethodsForEachStore.containsKey(storeId)) {

                    shippingMethodsForEachStore.put(storeId, new ArrayList<ShippingMethod>())

                }

                shippingMethodsForEachStore.get(storeId).add(shippingMethod)
            }
        }
        return shippingMethodsForEachStore
    }

    static HashMap<String,List<CartItem>> getCartsForEachStore(List<CartItem> cartItems) {

        HashMap<String,List<CartItem>> cartsForEachStore = new HashMap<>()

        for(CartItem cartItem in cartItems){

            String storeId =  cartItem.product_id.substring(0, 6)

            if (!cartsForEachStore.containsKey(storeId)) {
                cartsForEachStore.put(storeId,new ArrayList<CartItem>())
            }

            cartsForEachStore.get(storeId).add(cartItem.clone())
        }

        return cartsForEachStore

    }




    private static String getTotal(Order order) {
        List<Total> totalList = order.total

        String totalString = null

        if (totalList != null) {
            Total total = totalList.find({ it -> it.code.equals("total") })

            if (total != null) {
                totalString = total.value
            }

        }
        totalString
    }

    //Perfect
    protected static def getSpecName(String specName){
        return specName + "-" + ProjectConstants.orderSpecsIdForNifi
    }




    //Perfect
    protected static def getSpecName(String specName, String affiliateId){
        return affiliateId + "_" + specName + "-" + ProjectConstants.orderSpecsIdForNifi
    }

}
