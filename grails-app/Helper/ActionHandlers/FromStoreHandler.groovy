package ActionHandlers

import HelpingClasses.TopicSender
import Models.FirebaseModels.FromStorePacket
import Models.order.Order
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import orderenginev2.DBAffiliateOrder
import orderenginev2.StoreOrder

class FromStoreHandler {

    FromStorePacket fromStorePacket
    FromStoreHandler(FromStorePacket fromStorePacket){
        this.fromStorePacket = fromStorePacket
    }


    def handleActionFromStore(){

        StoreOrder dbStoreOrder

        if(fromStorePacket.id_to_return != null && !fromStorePacket.id_to_return.equals("")) {

            dbStoreOrder = StoreOrder.findById(fromStorePacket.id_to_return as Long)

        }

        else {

            dbStoreOrder = StoreOrder.findByStoreIdAndStoreOrderId(fromStorePacket.store_id,fromStorePacket.store_order_id)

        }

        if(dbStoreOrder != null) {
            try {
                Object tempOrder = new Gson().fromJson(fromStorePacket.order_json, new TypeToken<Order>() {}.getType())

                Order order = (Order) tempOrder

                dbStoreOrder.status = order.order_history.status

                dbStoreOrder.statusId = order.order_history.order_status_id

                dbStoreOrder.updatedAt = Calendar.getInstance().getTime().getTime() / 1000

                dbStoreOrder.storeOrderResponse = fromStorePacket.order_json

                dbStoreOrder.storeOrderId = fromStorePacket.store_order_id

                StoreOrder.withNewTransaction {
                    dbStoreOrder.save(flush: true)
                }


                updateAffiliate(dbStoreOrder)
            }
            catch (Exception e){
                e.printStackTrace()
            }
        }
        else{
            //place some error here related to storeOrder not found
        }
    }

    static def updateAffiliate(StoreOrder dbStoreOrder) {

        DBAffiliateOrder dbAffiliateOrder = DBAffiliateOrder.findByAffiliateIdAndAffiliateOrderId(dbStoreOrder.affiliateId,
                dbStoreOrder.affiliateOrderId)

        if(dbAffiliateOrder != null) {

            dbAffiliateOrder.status = dbStoreOrder.status

            dbAffiliateOrder.updatedAt = Calendar.getInstance().getTime().getTime()/1000

            dbAffiliateOrder.orderStatusId = dbStoreOrder.statusId

            dbAffiliateOrder.action = "updateOrder"

            DBAffiliateOrder.withNewTransaction {
                dbAffiliateOrder.save(flush:true)
            }

            sendToAffiliate(dbAffiliateOrder,dbStoreOrder)


        }
        else{
            //put some error here related to affiliate Order not found
        }

    }

    static def sendToAffiliate(DBAffiliateOrder dbAffiliateOrder, StoreOrder storeOrder) {

        HashMap<String,Object> recordValue = new HashMap<>()

        recordValue.put("action","updateOrder")

        recordValue.put("store_id",storeOrder.storeId)

        recordValue.put("affiliate_order_id",dbAffiliateOrder.affiliateOrderId)

        recordValue.put("status",storeOrder.status)

        recordValue.put("status_id",storeOrder.statusId)

        recordValue.put("order_json",new Gson().fromJson(storeOrder.storeOrderResponse,new TypeToken<HashMap<String,Object>>(){}.getType()))

        String payload = new Gson().toJson(recordValue)
        String partialTopicId = dbAffiliateOrder.affiliateId
        if(partialTopicId.equals("3843450"))
            partialTopicId =  partialTopicId + "_" + storeOrder.storeId

        TopicSender.postDataToTopic(payload,"affiliate_"+partialTopicId)

    }
}
