package orderenginev2

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DBAffiliateOrder {
    static mapWith = "mongo"
    @Expose
    @SerializedName("uuid")
    String uuid

    @Expose
    @SerializedName("compositeId")
    String compositeId

    @Expose
    @SerializedName("affiliateId")
    String affiliateId

    @Expose
    @SerializedName("affiliateOrderId")
    String affiliateOrderId

    @Expose
    @SerializedName("status")
    String status

    @Expose
    @SerializedName("amount")
    String amount

    @Expose
    @SerializedName("updatedAt")
    Long updatedAt

    @Expose
    @SerializedName("createdAt")
    Long createdAt

    @Expose
    @SerializedName("affiliateOrderObj")
    String affiliateOrderObj

    @Expose
    @SerializedName("orderStatusId")
    String orderStatusId

    @Expose
    @SerializedName("action")
    String action


    DBAffiliateOrder(){
        uuid = UUID.randomUUID().toString()
        updatedAt = Calendar.getInstance().getTime().getTime()/1000
        createdAt = Calendar.getInstance().getTime().getTime()/1000
    }

    DBAffiliateOrder(String affiliateId, String affiliateOrderId, String status, String order_status_id, String ammount, String affiliateOrderObj){
        compositeId = affiliateId + "_" + affiliateOrderId
        uuid = UUID.randomUUID().toString()

        println("affiliateId: " + affiliateId)
        println("affiliateOrderId: " + affiliateOrderId)
        println("status: " + status)
        println("ammount: " + ammount)
        println("affiliateOrderObj: " + affiliateOrderObj)

        this.affiliateId = affiliateId
        this.affiliateOrderId = affiliateOrderId
        this.status = status
        this.orderStatusId = order_status_id
        this.amount = ammount

        updatedAt = Calendar.getInstance().getTime().getTime()/1000
        createdAt = Calendar.getInstance().getTime().getTime()/1000

        this.affiliateOrderObj = affiliateOrderObj
    }



    static constraints = {
//        uuid(nullable: false, blank: false, editable:false)
//        compositeId(nullable: false, blank: false, editable:false)
//        affiliateId(blank: false, nullable: false)
//        affiliateOrderId(blank: false, nullable: false)
//        status()
//        amount()
//        affiliateOrderObj()
//        updatedAt(nullable: false)
//        createdAt(nullable: false, editable:false)
    }

}
