package orderenginev2

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.bson.types.ObjectId

class Check {



    @Expose
    @SerializedName("affiliateId")
    String affiliateId

    @Expose
    @SerializedName("affiliateOrderId")
    String affiliateOrderId
    static mapWith = "mongo"
    static constraints = {
    }
}
