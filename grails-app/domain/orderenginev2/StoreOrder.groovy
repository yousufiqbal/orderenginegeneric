package orderenginev2

class StoreOrder {

    static constraints = {
    }
    static mapWith = "mongo"


    StoreOrder(){
        uuid = UUID.randomUUID().toString()
        updatedAt = Calendar.getInstance().getTime().getTime()/1000
        createdAt = Calendar.getInstance().getTime().getTime()/1000
        storeOrderObj = ""
        storeOrderId=""
        storeOrderResponse=""
        compositeId=""

    }
    String uuid

    String compositeId // dbAffiliateOrderId _ storeId _ storeOrderId = Unique

    String affiliateId

    String affiliateOrderId

    String storeId

    String storeOrderId

    String status

    String statusId

    String storeOrderObj

    Long updatedAt

    Long createdAt

    String storeOrderResponse

}
